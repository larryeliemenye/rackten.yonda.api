var gulp = require('gulp');
var mocha = require('gulp-mocha');

gulp.task('run:unit-test', function(){
    return gulp.src('test/**/*.js').pipe(mocha({
        reporter:'progress'
    }))
});