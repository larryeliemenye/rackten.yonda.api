var mongoose = require('mongoose');
var autoInc = require('mongoose-auto-increment');
var config = require(__('config'));

var conString = "mongodb://127.0.0.1:27017/" + config.database.name

if(process.env.OPENSHIFT_MONGODB_DB_URL){
    conString = process.env.OPENSHIFT_MONGODB_DB_URL + config.database.name;
}
mongoose.connect(conString);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

mongoose.set('debug', true);
autoInc.initialize(db);
module.exports = mongoose;
