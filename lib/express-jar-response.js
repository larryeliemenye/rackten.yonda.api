/**
 * Created by larry Eliemenye
 * express JSON API Response spec
 * http://jsonapi.org/format/
 *
 */

var _ = require('lodash');

function JARSpec(options){
    this.meta = options.meta;
    this.app = options.app;
    this.res = options.res;
    this.req = options.req;
    this.next = options.next;

    //default content type
    this.res.setHeader('Content-Type','application/vnd.preptitude.api+json');

    if(this.req.headers['content-type']){
        var mimeType = this.req.headers['content-type'];
        if(containsParam(mimeType)){
            this.res.status(415).json({
                message:"Unsupported Media Type"
            })
        }

    }
}

var containsParam = function(mimeType){
    return (mimeType.indexOf(";") !== -1);
};

JARSpec.prototype.sendResponse = function(data, meta){
    console.log(arguments);
    this.res.json({
        data:data,
        meta: meta || this.meta
    });
};

JARSpec.prototype.sendError = function( error, meta){
    this.res.json({
        error:error,
        meta: meta || this.meta
    });
};

module.exports = function(options){
    return function(req, res, next){
        var defaultOptions = _.merge(options, {req: req, res: res, next:next});
        res.jarSpec = new JARSpec(defaultOptions);
        next()
    }
};