/**
 * Created by larry Eliemenye
 */

var request = require('supertest'),
    app = require('../../app');

var error = function(err, res){
    if(err) throw err;
};

describe('index Route', function(){
    it('Get /', function(done){
        request(app)
            .get('/')
            .expect('Content-Type', /json/)
            .expect(200, done);
    })
});
