/**
 * Created by larry Eliemenye
 */

var request = require('supertest'),
    app = require('../../app'),
    expect = require('chai').expect;

var error = function(err, res){
    if(err) throw err;
};

describe('Users Route', function(){
    it('Get /', function(done){
        request(app)
            .get('/')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

    it('Get /api/users/', function(done){
        request(app)
            .get('/api/users/')
            .expect('Content-Type', /json/)
            .expect(function(res){
                var result = JSON.parse(res.text);
                expect(result).to.have.property('data');
                expect(result.data).to.be.a('object');
                expect(result).to.have.property('meta');
            })
            .end(done);
    })
});