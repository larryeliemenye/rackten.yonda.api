/**
 * Created by larry Eliemenye
 */


var express = require('express');
var app = express();
require('./viewEngine') (app);
require('./session')    (app);
require('./parser')     (app);
require('./logger')     (app);
require('./router')     (app);

module.exports = app;