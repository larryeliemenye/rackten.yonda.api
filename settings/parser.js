var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var express = require('express');
var path = require('path');
var jar = require(__('lib/express-jar-response'));

module.exports = function(app){
    app.use(jar({
        express:app,
        models:[],
        meta:{
            apiVersion:"1.0",
            copyright:"Copyright 2015 Yonda",
            authors:[
                "Larry Eliemenye"
            ]
        }
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, '../', 'public'))); //no idea where to put this
};
