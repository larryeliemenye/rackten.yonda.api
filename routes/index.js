var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.jarSpec.sendResponse({ title: 'Yonda Rest APi' });
});

module.exports = router;
