var express = require('express');
var router = express.Router();

/* GET users listing. */

function index(req, res){
  res.jarSpec.sendResponse({name:"larry", age:30});
}

function createUser(req, res){
  res.jarSpec.sendResponse({message:'userCreated'});
}

router.route('/')
    .get(index);

router.route('/:id')
    .post(index);

module.exports = router;
