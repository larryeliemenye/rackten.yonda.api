/**
 * Created by larry Eliemenye
 */

var mongoose = require(__('lib/database'));
var timestamps = require('mongoose-concrete-timestamps');
var autoInc = require('mongoose-auto-increment');
var bcrypt = require('bcryptjs');

/**
 *
 * Schema Definition
 *
 * */

var UserSchema = new mongoose.Schema(
    {

        email: {type: String, unique: true},
        fullname: {type: String},
        password: {type: String},
        facebook: Number,
        instagram: {type: String}
    }
);

/**
 *
 * One ore more plugin to add to shcmea
 *
 * */

UserSchema.plugin(timestamps);
UserSchema.plugin(autoInc.plugin, {model: 'User', field: 'userId', startAt: 1000});

/**
 *
 * when password is requested, return hash
 *
 * */
UserSchema.virtual("password").get(function () {
    return this.hash;
});

/**
 *
 * When password is set, hash it.
 *
 * */

UserSchema.virtual("password").set(function (password) {
    //this is dangerous and should not be done when the server is running..need to find a way to make this async
    this.hash = bcrypt.hashSync(password, 10);
});

/**
 *
 * Exports only an instance of the schema's model
 *
 * */

module.exports = mongoose.model('User', UserSchema);
