/**
 * Created by larry Eliemenye
 */

/**
 * Created by larry Eliemenye
 */

var mongoose = require(__('lib/database'));
var timestamps = require('mongoose-concrete-timestamps');

/**
 *
 * Schema Definition
 *
 * */

var FlashbackSchema = new mongoose.Schema(
    {

        email: {type: String, unique: true},
        fullname: {type: String},
        password: {type: String},
        facebook: Number,
        instagram: {type: String}
    }
);

/**
 *
 * One ore more plugin to add to shcmea
 *
 * */

FlashbackSchema.plugin(timestamps);
FlashbackSchema.plugin(autoInc.plugin, {model: 'User', field: 'userId', startAt: 1000});


/**
 *
 * Exports only an instance of the schema's model
 *
 * */

module.exports = mongoose.model('Flashback', FlashbackSchema);
