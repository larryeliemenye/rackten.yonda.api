// handle requires from root
var path = require('path');
global.__ = function(file) {
    return path.join(__dirname, file);
};

global.abs_path = function(file){
    return __dirname + file
};

var appSettings = require('./settings');

module.exports = appSettings;